const http = require('http');

const port = 4000;

//create a variable server that stores the output of the create server method.
const server = http.createServer((request, response) => {


        //http://localhost:4000/greeting
      if(request.url == '/greeting') {
        response.writeHead(200,{'Content-Type': 'text/plain'}),
        response.end('Hello Again')
      }

      //Mini Activity
        else if(request.url == '/homepage') {
        response.writeHead(200,{'Content-Type': 'text/plain'}),
        response.end('This is the home Page! Welcome!')
      } else if (request.url == '/about') {
        response.writeHead(200,{'Content-Type': 'text/plain'}),
        response.end('Welcome to about page!')
      } else {
        response.writeHead(404,{'Content-Type': 'text/plain'}),   
        response.end('Page not found');
      }
});


//Use the server and port variable
server.listen(port);

//When server is running print this
console.log(`Server now accessible at localhost:${port}.`);