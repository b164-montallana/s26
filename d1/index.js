// Creat a basic server application by loading NODE.js modules.

//http >> hyper text transfer protocol
    //is a module to node that  transfer data. It is a set of individual files that contain code to create a component that helps establish data transfer between applications

//Clients (browser) and servers (node js/ express) communicate by exchanging individual messages.

//The message sent by the client, usually a web browser are called Request.

//The message sent by the server, calles Response.

let http = require("http");

//A port is a virtual point where network connection start and end.
//Each port is associated with a specific process or service
//default assign port 4000 via the listen (4000) method.
//http://localhost:4000


http.createServer(function(request, response) {

    //set status code for response - 200 == ok
    //set the content type
    response.writeHead(200, {'Content-Type': 'application/json'});
    response.end("Hello World")
    //response.end() to end the response process

}).listen(4000)

//When server is running, console will print the message:
console.log('Server running at localhost:4000');


/*
Status Code

100-199 => Informational responses
200-299 => Successful responses
300-399 => Redirection messages
400-499 => client error
500-599 => server error responses

*/